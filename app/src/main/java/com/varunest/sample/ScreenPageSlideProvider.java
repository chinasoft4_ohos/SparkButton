package com.varunest.sample;

import com.varunest.sparkbutton.SparkButton;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.*;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.utils.IntentConstants;
import ohos.utils.net.Uri;

import java.io.IOException;
import java.util.ArrayList;

/**
 * ScreenPageSlideProvider
 *
 * @author varun on 08/07/16.
 */
public class ScreenPageSlideProvider extends PageSliderProvider {
    private final AbilitySlice slice;
    private final ArrayList<Component> components;

    /**
     * 构造
     *
     * @param slice slice
     * @param components components
     */
    public ScreenPageSlideProvider(AbilitySlice slice, ArrayList<Component> components) {
        this.slice = slice;
        this.components = components;
    }

    /**
     * getCount
     *
     * @return int
     */
    @Override
    public int getCount() {
        return components.size();
    }

    /**
     * createPageInContainer
     *
     * @param componentContainer componentContainer
     * @param position position
     * @return Object
     */
    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int position) {
        Component component;
        switch (position) {
            case 0:
                component = components.get(0);
                setupStarLayoutClickEvents(component);
                break;
            case 1:
                component = components.get(1);
                setupHeartLayoutClickEvents(component);
                break;
            case 2:
                component = components.get(2);
                setupFacebookLayoutClickEvents(component);
                break;
            case 3:
                component = components.get(3);
                setupTwitterLayoutClickEvents(component);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + position);
        }
        component.setTag(String.valueOf(position));
        componentContainer.addComponent(component);
        return component;
    }

    /**
     * destroyPageFromContainer
     *
     * @param componentContainer componentContainer
     * @param index index
     * @param object object
     */
    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int index, Object object) {
        componentContainer.removeComponent((Component) object);
    }

    /**
     * isPageMatchToObject
     *
     * @param component component
     * @param object object
     * @return boolean
     */
    @Override
    public boolean isPageMatchToObject(Component component, Object object) {
        return component == object;
    }

    /**
     * getPageTitle
     *
     * @param position position
     * @return String
     */
    @Override
    public String getPageTitle(int position) {
        String title = "";
        switch (position) {
            case 1:
                title = slice.getString(ResourceTable.String_heart);
                break;
            case 2:
                title = slice.getString(ResourceTable.String_facebook);
                break;
            case 3:
                title = slice.getString(ResourceTable.String_twitter);
                break;
            default:
                title = slice.getString(ResourceTable.String_star);
                break;
        }
        return title;
    }

    private void setupStarLayoutClickEvents(final Component cp) {
        cp.findComponentById(ResourceTable.Id_github_page).setClickedListener(component -> openGithubPage());
    }

    private void setupHeartLayoutClickEvents(Component cp) {
        cp.findComponentById(ResourceTable.Id_github_page).setClickedListener(v -> openGithubPage());
    }

    private void setupFacebookLayoutClickEvents(Component cp) {
        cp.findComponentById(ResourceTable.Id_github_page).setClickedListener(v -> openGithubPage());
    }

    private void setupTwitterLayoutClickEvents(Component cp) {
        cp.findComponentById(ResourceTable.Id_github_page).setClickedListener(v -> openGithubPage());
        cp.findComponentById(ResourceTable.Id_twitter_card).setClickedListener(v -> {
            ((SparkButton) cp.findComponentById(ResourceTable.Id_twitter_button)).playAnimation();
            slice.getUITaskDispatcher().delayDispatch(this::openTwitterPage, 500);
        });
    }

    private void openGithubPage() {
        Intent intents = new Intent();
        try {
            String string = slice.getResourceManager().getElement(ResourceTable.String_gitee).getString();
            Operation operation = new Intent.OperationBuilder()
                .withUri(Uri.parse(string))
                .withBundleName("com.huawei.browser")
                .withAction(IntentConstants.ACTION_SEARCH)
                .build();
        intents.setOperation(operation);
        slice.startAbility(intents);
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
    }

    private void openTwitterPage() {
        Intent intents = new Intent();
        try {
            String string = slice.getResourceManager().getElement(ResourceTable.String_twitterh).getString();
            Operation operation = new Intent.OperationBuilder()
                .withUri(Uri.parse(string))
                .withBundleName("com.huawei.browser")
                .withAction(IntentConstants.ACTION_SEARCH)
                .build();
        intents.setOperation(operation);
        slice.startAbility(intents);
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
    }
}
