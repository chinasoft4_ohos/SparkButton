package com.varunest.sample;

import com.varunest.sparkbutton.SparkButton;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.util.ArrayList;

/**
 * DemoAbilitySlice
 */
public class DemoAbilitySlice extends AbilitySlice implements TabList.TabSelectedListener {
    private PageSlider showcasePageSlide;
    private PageSliderProvider pageSliderProvider;
    private TabList tabList;
    private ArrayList<Component> components;
    private boolean isOneClick = false;
    private boolean isTwoClick = false;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_demo);
        int color = 0;
        try {
            color = getResourceManager().getElement(ResourceTable.Color_colorPrimaryDark).getColor();
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.getMessage();
        }
        getWindow().setStatusBarColor(color);
        initWidgets();
        initAnimation();
    }

    private void initAnimation() {
        getUITaskDispatcher().delayDispatch(() -> {
            Component starLayout = components.get(0);
            if (starLayout != null) {
                playStarAnimation(starLayout);
            }
        }, 500);
    }

    private void initWidgets() {
        showcasePageSlide = (PageSlider) findComponentById(ResourceTable.Id_showcase_pageslide);
        components = new ArrayList<>();
        components.add(LayoutScatter.getInstance(this).parse(ResourceTable.Layout_demo_star, null, false));
        components.add(LayoutScatter.getInstance(this).parse(ResourceTable.Layout_demo_heart, null, false));
        components.add(LayoutScatter.getInstance(this).parse(ResourceTable.Layout_demo_facebook, null, false));
        components.add(LayoutScatter.getInstance(this).parse(ResourceTable.Layout_demo_twitter, null, false));

        pageSliderProvider = new ScreenPageSlideProvider(this, components);
        showcasePageSlide.setProvider(pageSliderProvider);
        showcasePageSlide.addPageChangedListener(getOnPageChangeListener());
        showcasePageSlide.setReboundEffect(true);
        showcasePageSlide.setCentralScrollMode(true);
        tabList = (TabList) findComponentById(ResourceTable.Id_tabs);
        tabList.setFixedMode(true);
        tabList.addTabSelectedListener(this);
        ArrayList<String> tabsTitle = new ArrayList<>();
        tabsTitle.add("STAR");
        tabsTitle.add("HEART");
        tabsTitle.add("FACEBOOK");
        tabsTitle.add("TWITTER");
        for (String title : tabsTitle) {
            TabList.Tab tab = tabList.new Tab(getContext());
            tab.setText(title);
            tab.setPadding(12, 0, 12, 0);
            tab.setMaxTextWidth(180);
            tabList.addTab(tab);
        }
        showcasePageSlide.setCurrentPage(0);
        tabList.selectTabAt(0);
    }

    private PageSlider.PageChangedListener getOnPageChangeListener() {
        return new PageSlider.PageChangedListener() {
            @Override
            public void onPageSliding(int i, float v, int i1) {
            }

            @Override
            public void onPageSlideStateChanged(int i) {
            }

            @Override
            public void onPageChosen(int position) {
                if (tabList.getSelectedTab().getPosition() != position) {
                    tabList.selectTabAt(position);
                }
                setAnimation(position);
            }
        };
    }

    private void setAnimation(int position) {
        Component component = components.get(position);
        if (component != null) {
            switch (position) {
                case 0:
                    playStarAnimation(component);
                    break;
                case 1:
                    playHeartAnimation(component);
                    break;
                case 2:
                    playFacebookAnimation(component);
                    break;
                case 3:
                    playTwitterAnimation(component);
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    private void playHeartAnimation(Component heartLayout) {
        getUITaskDispatcher().delayDispatch(new Runnable() {
            @Override
            public void run() {
                ((SparkButton) heartLayout.findComponentById(ResourceTable.Id_heart_button)).setChecked(true);
                ((SparkButton) heartLayout.findComponentById(ResourceTable.Id_heart_button)).playAnimation();
            }
        }, 300);
    }

    private void playStarAnimation(Component starLayout) {
        SparkButton button = (SparkButton) starLayout.findComponentById(ResourceTable.Id_star_button1);
        SparkButton button1 = (SparkButton) starLayout.findComponentById(ResourceTable.Id_star_button2);
        starLayout.findComponentById(ResourceTable.Id_cardview_1).setClickedListener(component -> {
            button.setChecked(!isOneClick);
            isOneClick = !isOneClick;
            button.playAnimation();
        });
        starLayout.findComponentById(ResourceTable.Id_cardview_2).setClickedListener(component -> {
            button1.setChecked(!isTwoClick);
            isTwoClick = !isTwoClick;
            button1.playAnimation();
        });
        ((SparkButton) starLayout.findComponentById(ResourceTable.Id_star_button1)).setChecked(false);
        isOneClick = false;
        ((SparkButton) starLayout.findComponentById(ResourceTable.Id_star_button2)).setChecked(true);
        isTwoClick = true;
        ((SparkButton) starLayout.findComponentById(ResourceTable.Id_star_button2)).playAnimation();
    }

    private void playFacebookAnimation(Component facebookLayout) {
        ((SparkButton) facebookLayout.findComponentById(ResourceTable.Id_facebook_button)).setChecked(true);
        ((SparkButton) facebookLayout.findComponentById(ResourceTable.Id_facebook_button)).playAnimation();
    }

    private void playTwitterAnimation(Component twitterLayout) {
        ((SparkButton) twitterLayout.findComponentById(ResourceTable.Id_twitter_button)).setChecked(true);
        ((SparkButton) twitterLayout.findComponentById(ResourceTable.Id_twitter_button)).playAnimation();
    }

    /**
     * onSelected
     *
     * @param tab tab
     */
    @Override
    public void onSelected(TabList.Tab tab) {
        if (showcasePageSlide.getCurrentPage() != tab.getPosition()) {
            showcasePageSlide.setCurrentPage(tab.getPosition());
            setAnimation(tab.getPosition());
        }
    }

    /**
     * onUnselected
     *
     * @param tab tab
     */
    @Override
    public void onUnselected(TabList.Tab tab) {
    }

    /**
     * onReselected
     *
     * @param tab tab
     */
    @Override
    public void onReselected(TabList.Tab tab) {
    }
}
