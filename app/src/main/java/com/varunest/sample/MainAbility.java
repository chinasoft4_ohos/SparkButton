package com.varunest.sample;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

/**
 * MainAbility
 */
public class MainAbility extends Ability {
    /**
     * onStart
     *
     * @param intent intent
     */
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(DemoAbilitySlice.class.getName());
    }
}
