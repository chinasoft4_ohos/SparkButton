package com.varunest.sample;

import com.varunest.sparkbutton.helper.Utils;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * ExampleOhosTest
 */
public class ExampleOhosTest {
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.varunest.sample", actualBundleName);
    }

    @Test
    public void clamp() {
        double clamp = Utils.clamp(1, 2, 3);
        double min = Math.min(Math.max(1, 2), 3);
        assert clamp == min;
    }
}