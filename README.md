# SparkButton

#### 项目介绍

- 项目名称：SparkButton
- 所属系列：openharmony的第三方组件适配移植
- 功能：一个拥有与Twitter心跳动画类似效果的按钮
- 项目移植状态：主功能完成
- 调用差异：无
- 基线版本：Release 1.0.6
- 开发版本：sdk6，DevEco Studio 2.2 Beta1

#### 效果演示
<img src="image/gif1.gif"></img>

#### 安装教程
1、在项目根目录下的build.gradle文件中
 ```gradle
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```
2.在entry模块的build.gradle文件中
 ```gradle
 dependencies {
    implementation('com.gitee.chinasoft_ohos:SparkButton:1.0.0')
    ......  
 }
 ```
在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行,如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

1、xml
 ```xml
   <com.varunest.sparkbutton.SparkButton
        ohos:id="$+id:facebook_button"
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:layout_alignment="center"
        ohos:margin="30vp"
        ohos:top_padding="10vp"
        app:sparkbutton_flag="facebook"
        app:sparkbutton_iconSize="250"
        app:sparkbutton_pressOnTouch="false"
        app:sparkbutton_primaryColor="#dfe3ee"
        app:sparkbutton_secondaryColor="#3b5998"/>
 ```
2、java
```
SparkButton button  = new SparkButtonBuilder(context)
                .setImageFlag("heart")
                .setImageSizePx(20)
                .setPrimaryColor("#FFC107")
                .setSecondaryColor("#FF5722")
                .build();     
```
3、自定义属性
**SparkButton**

| 名称 | 类型 |说明 |
|---|---|---|
|sparkbutton_flag|String|图片标记|
|sparkbutton_iconSize|int|图片尺寸|
|sparkbutton_pressOnTouch|boolean|touch|
|sparkbutton_primaryColor|String|起始颜色|
|sparkbutton_secondaryColor|String|结束颜色|
#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.0

#### 版权和许可信息
```
Library falls under [Apache 2.0] (LICENSE.md)
```