## 1.0.0
ohos 第三个版本
* 正式版本

## 0.0.2-SNAPSHOT
* 修改FindBugs

## 0.0.1-SNAPSHOT
ohos第一个版本
* 实现了原库的大部分api
* 因为没有相关api,自定义属性中图片资源通过资源id传入功能未实现