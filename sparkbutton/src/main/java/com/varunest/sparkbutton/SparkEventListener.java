package com.varunest.sparkbutton;

import ohos.agp.components.Image;

/**
 * SparkEventListener
 *
 * @author varun on 07/07/16.
 */
public interface SparkEventListener {
    /**
     * onEvent
     *
     * @param button button
     * @param isButtonState isButtonState
     */
    void onEvent(Image button, boolean isButtonState);

    /**
     * onEventAnimationEnd
     *
     * @param button button
     * @param isButtonState isButtonState
     */
    void onEventAnimationEnd(Image button, boolean isButtonState);

    /**
     * onEventAnimationStart
     *
     * @param button button
     * @param isButtonState isButtonState
     */
    void onEventAnimationStart(Image button, boolean isButtonState);
}
