package com.varunest.sparkbutton.helper;

import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.util.Optional;

/**
 * Utils
 */
public class Utils {
    /**
     * mapValueFromRangeToRange
     *
     * @param value value
     * @param fromLow fromLow
     * @param fromHigh fromHigh
     * @param toLow toLow
     * @param toHigh toHigh
     * @return double
     */
    public static double mapValueFromRangeToRange(
            double value, double fromLow, double fromHigh, double toLow, double toHigh) {
        return toLow + ((value - fromLow) / (fromHigh - fromLow) * (toHigh - toLow));
    }

    /**
     * clamp
     *
     * @param value value
     * @param low low
     * @param high high
     * @return double
     */
    public static double clamp(double value, double low, double high) {
        return Math.min(Math.max(value, low), high);
    }

    /**
     * dpToPx
     *
     * @param context context
     * @param dp dp
     * @return int
     */
    public static int dpToPx(Context context, int dp) {
        Optional<Display> display = DisplayManager.getInstance().getDefaultDisplay(context);
        DisplayAttributes displayAttributes = display.get().getAttributes();
        return Math.round(dp * (displayAttributes.xDpi / DisplayAttributes.MEDIUM_DENSITY));
    }
}