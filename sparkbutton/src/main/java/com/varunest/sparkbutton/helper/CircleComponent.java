package com.varunest.sparkbutton.helper;

import com.varunest.sparkbutton.custom.TransceiverUtil;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * CircleComponent
 *
 * @author Miroslaw Stanek on 21.12.2015.
 * @modifier Varun
 */
public class CircleComponent extends Component implements Component.EstimateSizeListener, Component.DrawTask {
    private Paint circlePaint = new Paint();
    private Paint maskPaint = new Paint();
    private int width;
    private int height;

    private float outerCircleRadiusProgress = 0f;
    private float innerCircleRadiusProgress = 0f;
    private int startColor;
    private int endColor;

    /**
     * CircleComponent
     *
     * @param context context
     */
    public CircleComponent(Context context) {
        super(context);
        addDrawTask(this);
        setEstimateSizeListener(this);
        init();
    }

    /**
     * CircleComponent
     *
     * @param context context
     * @param attrs attrs
     */
    public CircleComponent(Context context, AttrSet attrs) {
        super(context, attrs);
        addDrawTask(this);
        setEstimateSizeListener(this);
        init();
    }

    /**
     * CircleComponent
     *
     * @param context context
     * @param attrs attrs
     * @param defStyleAttr defStyleAttr
     */
    public CircleComponent(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        addDrawTask(this);
        setEstimateSizeListener(this);
        init();
    }

    private void init() {
        circlePaint.setStyle(Paint.Style.STROKE_STYLE);
        maskPaint.setStyle(Paint.Style.FILLANDSTROKE_STYLE);
        maskPaint.setColor(Color.RED);
        circlePaint.setColor(Color.RED);
        circlePaint.setStrokeWidth(10);
        maskPaint.setStrokeWidth(10);
    }

    /**
     * setInnerCircleRadiusProgress
     *
     * @param innerCircleRadiusProgress innerCircleRadiusProgress
     */
    public void setInnerCircleRadiusProgress(float innerCircleRadiusProgress) {
        this.innerCircleRadiusProgress = innerCircleRadiusProgress;
        updateCircleColor();
        updateDotsAlpha();
        invalidate();
    }

    /**
     * getInnerCircleRadiusProgress
     *
     * @return float
     */
    public float getInnerCircleRadiusProgress() {
        return innerCircleRadiusProgress;
    }

    /**
     * getOuterCircleRadiusProgress
     *
     * @return float
     */
    public float getOuterCircleRadiusProgress() {
        return outerCircleRadiusProgress;
    }

    /**
     * setOuterCircleRadiusProgress
     *
     * @param outerCircleRadiusProgress outerCircleRadiusProgress
     */
    public void setOuterCircleRadiusProgress(float outerCircleRadiusProgress) {
        this.outerCircleRadiusProgress = outerCircleRadiusProgress;
        updateCircleColor();
        updateDotsAlpha();
        invalidate();
    }

    private void updateDotsAlpha() {
        float colorProgress = (float) Utils.clamp(outerCircleRadiusProgress, 0.5, 1);
        colorProgress = (float) Utils.mapValueFromRangeToRange(colorProgress, 0.5f, 1f, 0f, 1f);
        maskPaint.setAlpha((float) (1d - colorProgress));
        circlePaint.setAlpha((float) (1d - colorProgress));
    }

    private void updateCircleColor() {
        float colorProgress = (float) Utils.clamp(outerCircleRadiusProgress, 0.5, 1);
        colorProgress = (float) Utils.mapValueFromRangeToRange(colorProgress, 0.5f, 1f, 0f, 1f);
        TransceiverUtil util = new TransceiverUtil(startColor, endColor);
        this.circlePaint.setColor(new Color(util.getColor(colorProgress)));
        this.maskPaint.setColor(new Color(endColor));
        circlePaint.setStrokeWidth((float) (10d * (1d - colorProgress)));
        maskPaint.setStrokeWidth((float) (10d * (1d - colorProgress)));
    }

    /**
     * onDraw
     *
     * @param component component
     * @param canvas canvas
     */
    @Override
    public void onDraw(Component component, Canvas canvas) {
        canvas.drawCircle((float) (getWidth() / 2d), (float) (getHeight() / 2d), (float) (outerCircleRadiusProgress * (width / 2d)), circlePaint);
        canvas.drawCircle((float) (getWidth() / 2d), (float) (getHeight() / 2d), (float) (innerCircleRadiusProgress * (width / 2d)), maskPaint);
    }

    /**
     * onEstimateSize
     *
     * @param widthEstimateConfig widthEstimateConfig
     * @param heightEstimateConfig heightEstimateConfig
     * @return boolean
     */
    @Override
    public boolean onEstimateSize(int widthEstimateConfig, int heightEstimateConfig) {
        width = EstimateSpec.getSize(widthEstimateConfig);
        height = EstimateSpec.getSize(heightEstimateConfig);
        setEstimatedSize(
                EstimateSpec.getChildSizeWithMode(width, width, EstimateSpec.NOT_EXCEED),
                EstimateSpec.getChildSizeWithMode(height, height, EstimateSpec.NOT_EXCEED));
        return true;
    }

    /**
     * setColors
     *
     * @param startColor startColor
     * @param endColor endColor
     */
    public void setColors(int startColor, int endColor) {
        this.startColor = startColor;
        this.endColor = endColor;
    }
}