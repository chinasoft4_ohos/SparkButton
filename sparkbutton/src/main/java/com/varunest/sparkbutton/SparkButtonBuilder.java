package com.varunest.sparkbutton;

import com.varunest.sparkbutton.helper.Utils;
import ohos.app.Context;

/**
 * SparkButtonBuilder
 *
 * @author varun on 07/07/16.
 */
public class SparkButtonBuilder {
    private SparkButton sparkButton;
    private Context context;

    /**
     * SparkButtonBuilder
     *
     * @param context context
     */
    public SparkButtonBuilder(Context context) {
        this.context = context;
        sparkButton = new SparkButton(context);
    }

    /**
     * setActiveImage
     *
     * @param resourceId resourceId
     * @return SparkButtonBuilder
     */
    public SparkButtonBuilder setActiveImage(int resourceId) {
        sparkButton.imageResourceIdActive = resourceId;
        return this;
    }

    /**
     * setInactiveImage
     *
     * @param resourceId resourceId
     * @return SparkButtonBuilder
     */
    public SparkButtonBuilder setInactiveImage(int resourceId) {
        sparkButton.imageResourceIdInactive = resourceId;
        return this;
    }

    /**
     * setPrimaryColor
     *
     * @param color color
     * @return SparkButtonBuilder
     */
    public SparkButtonBuilder setPrimaryColor(int color) {
        sparkButton.primaryColor = color;
        return this;
    }

    /**
     * setSecondaryColor
     *
     * @param color color
     * @return SparkButtonBuilder
     */
    public SparkButtonBuilder setSecondaryColor(int color) {
        sparkButton.secondaryColor = color;
        return this;
    }

    /**
     * setImageSizePx
     *
     * @param px px
     * @return SparkButtonBuilder
     */
    public SparkButtonBuilder setImageSizePx(int px) {
        sparkButton.imageSize = px;
        return this;
    }

    /**
     * setImageSizeDp
     *
     * @param dp dp
     * @return SparkButtonBuilder
     */
    public SparkButtonBuilder setImageSizeDp(int dp) {
        sparkButton.imageSize = Utils.dpToPx(context, dp);
        return this;
    }

    /**
     * setAnimationSpeed
     *
     * @param value value
     * @return SparkButtonBuilder
     */
    public SparkButtonBuilder setAnimationSpeed(float value) {
        sparkButton.animationSpeed = value;
        return this;
    }

    /**
     * build
     *
     * @return SparkButton
     */
    public SparkButton build() {
        sparkButton.init();
        return sparkButton;
    }
}
