/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.varunest.sparkbutton.custom;

import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;

import java.util.NoSuchElementException;

/**
 * TypedAttrUtils
 */
public final class TypedAttrUtils {
    /**
     * getBoolean
     *
     * @param attrs attrs
     * @param attrName attrName
     * @param isDefValue isDefValue
     * @return boolean
     */
    public static boolean getBoolean(AttrSet attrs, String attrName, boolean isDefValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return isDefValue;
        } else {
            return attr.getBoolValue();
        }
    }

    /**
     * getInteger
     *
     * @param attrs attrs
     * @param attrName attrName
     * @param defValue defValue
     * @return int
     */
    public static int getInteger(AttrSet attrs, String attrName, int defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getIntegerValue();
        }
    }

    /**
     * getString
     *
     * @param attrs attrs
     * @param attrName attrName
     * @param defValue defValue
     * @return String
     */
    public static String getString(AttrSet attrs, String attrName, String defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getStringValue();
        }
    }

    /**
     * getDimensionPixelSize
     *
     * @param attrs attrs
     * @param attrName attrName
     * @param defValue defValue
     * @return int
     */
    public static int getDimensionPixelSize(AttrSet attrs, String attrName, int defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getIntegerValue();
        }
    }

    /**
     * attrNoSuchElement
     *
     * @param attrs attrs
     * @param attrName attrName
     * @return Attr
     */
    private static Attr attrNoSuchElement(AttrSet attrs, String attrName) {
        Attr attr = null;
        try {
            attr = attrs.getAttr(attrName).get();
        } catch (NoSuchElementException e) {
            e.getMessage();
        }
        return attr;
    }
}
