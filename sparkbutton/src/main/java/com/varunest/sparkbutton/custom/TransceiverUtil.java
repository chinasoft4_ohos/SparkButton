/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.varunest.sparkbutton.custom;

import ohos.agp.colors.RgbColor;
import ohos.agp.utils.Color;

/**
 * TransceiverUtil
 */
public class TransceiverUtil {
    private int mStartColor;
    private int mEndColor;

    /***
     * 构造
     *
     * @param startColor 开始颜色
     * @param endColor 结束颜色
     */
    public TransceiverUtil(int startColor, int endColor) {
        this.mStartColor = startColor;
        this.mEndColor = endColor;
    }

    /**
     * setStartColor
     *
     * @param startColor startColor
     */
    public void setStartColor(int startColor) {
        this.mStartColor = startColor;
    }

    /**
     * setEndColor
     *
     * @param endColor endColor
     */
    public void setEndColor(int endColor) {
        this.mEndColor = endColor;
    }

    /**
     * 获取某一个百分比间的颜色,radio取值[0,1]
     *
     * @param radio 颜色值
     * @return 颜色
     */
    public int getColor(float radio) {
        int redStart = red(mStartColor);
        int blueStart = blue(mStartColor);
        int greenStart = green(mStartColor);
        int redEnd = red(mEndColor);
        int blueEnd = blue(mEndColor);
        int greenEnd = green(mEndColor);

        int red = (int) (redStart + ((redEnd - redStart) * radio + 0.5));
        int greed = (int) (greenStart + ((greenEnd - greenStart) * radio + 0.5));
        int blue = (int) (blueStart + ((blueEnd - blueStart) * radio + 0.5));
        return Color.argb(255,red, greed, blue);
    }

    /**
     * getRgbColor
     *
     * @param radio radio
     * @return RgbColor
     */
    public RgbColor getRgbColor(float radio) {
        int redStart = red(mStartColor);
        int blueStart = blue(mStartColor);
        int greenStart = green(mStartColor);
        int redEnd = red(mEndColor);
        int blueEnd = blue(mEndColor);
        int greenEnd = green(mEndColor);
        int red = (int) (redStart + ((redEnd - redStart) * radio + 0.5));
        int greed = (int) (greenStart + ((greenEnd - greenStart) * radio + 0.5));
        int blue = (int) (blueStart + ((blueEnd - blueStart) * radio + 0.5));
        return new RgbColor(red, greed, blue);
    }

    /**
     * 获取颜色中的红色
     *
     * @param color 颜色
     * @return int int类型
     */
    public int red(int color) {
        return (color >> 16) & 0xFF;
    }

    /**
     * 获取颜色中的绿色
     *
     * @param color 颜色
     * @return int int类型
     */
    public int green(int color) {
        return (color >> 8) & 0xFF;
    }

    /**
     * 获取颜色中的蓝色
     *
     * @param color 颜色
     * @return int int类型
     */
    public int blue(int color) {
        return color & 0xFF;
    }
}
